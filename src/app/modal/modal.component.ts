import { HttpHeaders } from '@angular/common/http';
import { Component, Input, OnDestroy, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AddRoleGQL, QueryGQL, QueryQuery } from '../services/generated-frontend';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.sass']
})
export class ModalComponent implements OnInit, OnDestroy {
  @Input("idName")
  public id: string = "";

  @Output()
  public addRoleEvent = new EventEmitter<{
    selectedUnit:  string,
    selectedTitle: string
  }>();

  public units:  QueryQuery['units'];
  public titles: QueryQuery['titles'];
  public group = this.formBuilder.group({
    selectedUnit:  [],
    selectedTitle: []
  });
  public wrongSelection = "";

  private subscription = new Subscription();
  
  @ViewChild('closeButton')
  closeButton : ElementRef | null = null;

  constructor(
    private formBuilder: FormBuilder,
    private queryService: QueryGQL,
    private addRoleService: AddRoleGQL
  ) { }

  ngOnInit(): void {
    this.subscription = this.queryService.watch().valueChanges
      .subscribe((response) => {
        this.units  = response.data.units;
        this.titles = response.data.titles;
      });
    this.group.valueChanges
      .subscribe(_ => {
        this.wrongSelection = "";
      })
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  onSaveChanges(): void {
    const unit  = this.group.get('selectedUnit');
    const title = this.group.get('selectedTitle');
    if (unit && unit.value && title && title.value) {
      this.addRoleEvent.emit({
        selectedUnit:  unit.value.id,
        selectedTitle: title.value.id
      });
      const handle = this.closeButton?.nativeElement as HTMLElement;
      handle.click();
    } else {
      if (!unit || !unit.value) {
        this.wrongSelection = "unit";
      } else {
        this.wrongSelection = "title";
      }
    }
  }
}
