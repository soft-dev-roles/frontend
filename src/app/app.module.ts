import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { SearchPipe } from './search.pipe';
import { GraphQLModule } from './graphql.module';
import { DropdownComponent } from './dropdown/dropdown.component';
import { HomeComponent } from './home/home.component';
import { StaffComponent } from './staff/staff.component';
import { NotInListPipe } from './not-in-list.pipe';
import { TableComponent } from './table/table.component';
import { RolesAccordionComponent } from './roles-accordion/roles-accordion.component';
import { RolesTabsComponent } from './roles-tabs/roles-tabs.component';
import { TabContentComponent } from './tab-content/tab-content.component';
import { ModalComponent } from './modal/modal.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: ':apmId',  component: StaffComponent},
  { path: '', redirectTo: '/home', pathMatch: 'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    DropdownComponent,
    HomeComponent,
    StaffComponent,
    RolesAccordionComponent,
    TableComponent,
    RolesTabsComponent,
    TabContentComponent,
    ModalComponent,
    SearchPipe,
    NotInListPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot(routes),
    GraphQLModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
