import { Component, ContentChild, Input, OnInit, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-roles-accordion',
  templateUrl: './roles-accordion.component.html',
  styleUrls: ['./roles-accordion.component.sass']
})
export class RolesAccordionComponent implements OnInit {
  @ContentChild('bodyTemplate', { static: false })
  bodyRef: TemplateRef<any> | null = null;

  @Input() id = "";
  @Input() data: any;
  @Input() header: string[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
