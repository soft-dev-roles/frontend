import { gql } from 'apollo-angular';
import { Injectable } from '@angular/core';
import * as Apollo from 'apollo-angular';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  DateTime: Date;
};

export type Mutation = {
  __typename?: 'Mutation';
  addRole?: Maybe<Roles>;
  createTitle?: Maybe<Scalars['DateTime']>;
  expireRole?: Maybe<RolesExpired>;
  restoreRole?: Maybe<Roles>;
};


export type MutationAddRoleArgs = {
  apmId: Scalars['String'];
  role: Scalars['ID'];
  unitId: Scalars['ID'];
};


export type MutationCreateTitleArgs = {
  lastUpdated: Scalars['DateTime'];
};


export type MutationExpireRoleArgs = {
  id: Scalars['Int'];
};


export type MutationRestoreRoleArgs = {
  id: Scalars['Int'];
};

export type Query = {
  __typename?: 'Query';
  roles?: Maybe<Array<Maybe<Roles>>>;
  staff?: Maybe<Array<Maybe<Staff>>>;
  staff2?: Maybe<Array<Maybe<Staff>>>;
  titles?: Maybe<Array<Maybe<Title>>>;
  units?: Maybe<Array<Maybe<Unit>>>;
};


export type QueryStaffArgs = {
  apmId?: InputMaybe<Scalars['String']>;
};


export type QueryStaff2Args = {
  name?: InputMaybe<Scalars['String']>;
  titleId?: InputMaybe<Array<Scalars['ID']>>;
  unitId?: InputMaybe<Scalars['ID']>;
};

export type Roles = {
  __typename?: 'Roles';
  apmId: Scalars['String'];
  deletedBy?: Maybe<Scalars['String']>;
  end?: Maybe<Scalars['DateTime']>;
  id: Scalars['Int'];
  lastUpdated: Scalars['DateTime'];
  role: Scalars['String'];
  staff?: Maybe<Staff>;
  start?: Maybe<Scalars['DateTime']>;
  title: Title;
  unit: Unit;
  unitId: Scalars['String'];
  updatedBy?: Maybe<Scalars['String']>;
};

export type RolesExpired = {
  __typename?: 'RolesExpired';
  apmId: Scalars['String'];
  end?: Maybe<Scalars['DateTime']>;
  expirationDate: Scalars['DateTime'];
  id: Scalars['Int'];
  role: Scalars['String'];
  staff?: Maybe<Staff>;
  start?: Maybe<Scalars['DateTime']>;
  title: Title;
  unit: Unit;
  unitId: Scalars['String'];
  updatedBy?: Maybe<Scalars['String']>;
};

export type Staff = {
  __typename?: 'Staff';
  adminId: Scalars['String'];
  afm?: Maybe<Scalars['String']>;
  amka?: Maybe<Scalars['String']>;
  apmId: Scalars['String'];
  apmIdNew?: Maybe<Scalars['String']>;
  birthdate?: Maybe<Scalars['DateTime']>;
  comment?: Maybe<Scalars['String']>;
  end?: Maybe<Scalars['DateTime']>;
  first: Scalars['String'];
  firstEn?: Maybe<Scalars['String']>;
  gender: Staff_Gender;
  homeAddress?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  last: Scalars['String'];
  lastEn?: Maybe<Scalars['String']>;
  lastModifiedAtSource?: Maybe<Scalars['DateTime']>;
  lastSync?: Maybe<Scalars['DateTime']>;
  lastUpdated: Scalars['DateTime'];
  middle?: Maybe<Scalars['String']>;
  middleEn?: Maybe<Scalars['String']>;
  nationalityId?: Maybe<Scalars['String']>;
  roles?: Maybe<Array<Roles>>;
  roles_expired?: Maybe<Array<RolesExpired>>;
  start?: Maybe<Scalars['DateTime']>;
  status?: Maybe<Staff_Status>;
  subUnitId?: Maybe<Scalars['String']>;
  telFax?: Maybe<Scalars['String']>;
  telHome?: Maybe<Scalars['String']>;
  telMobile?: Maybe<Scalars['String']>;
  telOffice?: Maybe<Scalars['String']>;
  titleId: Scalars['String'];
  unit?: Maybe<Unit>;
  unitDetails?: Maybe<Scalars['String']>;
  unitId: Scalars['String'];
};

export type Title = {
  __typename?: 'Title';
  affiliation?: Maybe<Title_Affiliation>;
  assignedMoreThanOne?: Maybe<Scalars['Boolean']>;
  id: Scalars['ID'];
  lastUpdated: Scalars['DateTime'];
  manualRole?: Maybe<Scalars['Boolean']>;
  nameEn: Scalars['String'];
  nameFemale: Scalars['String'];
  nameMale: Scalars['String'];
  publicRole?: Maybe<Scalars['Boolean']>;
  pyka_tablename?: Maybe<Scalars['String']>;
  relUnitType?: Maybe<Title_RelUnitType>;
  renewalYears?: Maybe<Scalars['Int']>;
  roleOrder?: Maybe<Scalars['Int']>;
  roles?: Maybe<Array<Roles>>;
  shortName?: Maybe<Scalars['String']>;
  titleCategory?: Maybe<Title_TitleCategory>;
  type?: Maybe<Title_Type>;
  unitEditor?: Maybe<Scalars['Boolean']>;
};

export type Unit = {
  __typename?: 'Unit';
  adminUnitId: Scalars['String'];
  adminUnitIdFormatted: Scalars['String'];
  adminUnitIdNew?: Maybe<Scalars['String']>;
  authURL?: Maybe<Scalars['String']>;
  authURLEn?: Maybe<Scalars['String']>;
  dnsAdmin?: Maybe<Scalars['String']>;
  dns_admin_email?: Maybe<Scalars['String']>;
  domain: Scalars['String'];
  id: Scalars['ID'];
  isAcademic: Unit_IsAcademic;
  isStem?: Maybe<Unit_IsStem>;
  ldapOU?: Maybe<Scalars['String']>;
  mail?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  nameEn?: Maybe<Scalars['String']>;
  noAccountCreate?: Maybe<Unit_NoAccountCreate>;
  orderByAthena?: Maybe<Scalars['Int']>;
  parentDomain?: Maybe<Scalars['String']>;
  parentUnitId?: Maybe<Scalars['Int']>;
  primDns?: Maybe<Unit_PrimDns>;
  roles?: Maybe<Array<Roles>>;
  secretaryPersonId?: Maybe<Scalars['Int']>;
  secretaryTels?: Maybe<Scalars['String']>;
  staff?: Maybe<Array<Staff>>;
  syncAdmin?: Maybe<Unit_SyncAdmin>;
  tStamp: Scalars['DateTime'];
  type?: Maybe<Unit_Type>;
  unitStatus: Unit_UnitStatus;
  url?: Maybe<Scalars['String']>;
};

export enum Staff_Gender {
  F = 'F',
  M = 'M'
}

export enum Staff_Status {
  Active = 'active',
  Deceased = 'deceased',
  Departed = 'departed',
  Retired = 'retired',
  Suspended = 'suspended',
  TransferOut = 'transferOut',
  Undetermined = 'undetermined',
  Unknown = 'unknown'
}

export enum Title_Affiliation {
  Affiliate = 'affiliate',
  Employee = 'employee',
  Faculty = 'faculty',
  Staff = 'staff',
  Student = 'student'
}

export enum Title_RelUnitType {
  AdminDept = 'adminDept',
  AdminDir = 'adminDir',
  AdminDirGen = 'adminDirGen',
  AdminOffice = 'adminOffice',
  Committee = 'committee',
  Department = 'department',
  Faculty = 'faculty',
  FacultyDeansOffice = 'facultyDeansOffice',
  Lab = 'lab',
  Rectorship = 'rectorship',
  School = 'school',
  Senate = 'senate',
  Unit = 'unit'
}

export enum Title_TitleCategory {
  Affiliate = 'affiliate',
  PermAdminStaff = 'permAdminStaff',
  PermAssistStaff = 'permAssistStaff',
  PermFaculty = 'permFaculty',
  Student = 'student',
  TempFaculty = 'tempFaculty',
  TempStaff = 'tempStaff'
}

export enum Title_Type {
  InactivEtitle = 'INACTIVEtitle',
  Role = 'role',
  Staff = 'staff',
  Student = 'student',
  TempStaff = 'tempStaff'
}

export enum Unit_IsAcademic {
  Off = 'off',
  On = 'on'
}

export enum Unit_IsStem {
  Off = 'off',
  On = 'on'
}

export enum Unit_NoAccountCreate {
  Off = 'off',
  On = 'on'
}

export enum Unit_PrimDns {
  Off = 'off',
  On = 'on'
}

export enum Unit_SyncAdmin {
  Active = 'active',
  Inactive = 'inactive'
}

export enum Unit_Type {
  AdminDept = 'adminDept',
  AdminDir = 'adminDir',
  AdminDirGen = 'adminDirGen',
  AdminOffice = 'adminOffice',
  Committee = 'committee',
  Department = 'department',
  Diner = 'diner',
  DoorGuard = 'doorGuard',
  Faculty = 'faculty',
  FacultyDeansOffice = 'facultyDeansOffice',
  GradProgram = 'gradProgram',
  IndepEntity = 'indepEntity',
  Lab = 'lab',
  Library = 'library',
  Rectorship = 'rectorship',
  School = 'school',
  Senate = 'senate',
  ThirdParty = 'thirdParty',
  Unit = 'unit'
}

export enum Unit_UnitStatus {
  Active = 'active',
  Inactive = 'inactive'
}

export type QueryQueryVariables = Exact<{ [key: string]: never; }>;


export type QueryQuery = { __typename?: 'Query', staff?: Array<{ __typename?: 'Staff', apmId: string, firstEn?: string | null | undefined, lastEn?: string | null | undefined } | null | undefined> | null | undefined, units?: Array<{ __typename?: 'Unit', id: string, name: string, nameEn?: string | null | undefined } | null | undefined> | null | undefined, titles?: Array<{ __typename?: 'Title', id: string, nameEn: string } | null | undefined> | null | undefined };

export type SearchQueryVariables = Exact<{
  unitId?: InputMaybe<Scalars['ID']>;
  titleId?: InputMaybe<Array<Scalars['ID']> | Scalars['ID']>;
  name?: InputMaybe<Scalars['String']>;
}>;


export type SearchQuery = { __typename?: 'Query', staff2?: Array<{ __typename?: 'Staff', apmId: string, firstEn?: string | null | undefined, lastEn?: string | null | undefined, first: string, last: string, roles?: Array<{ __typename?: 'Roles', role: string, unit: { __typename?: 'Unit', name: string, nameEn?: string | null | undefined } }> | null | undefined } | null | undefined> | null | undefined };

export type StaffWithApmQueryVariables = Exact<{
  apmId: Scalars['String'];
}>;


export type StaffWithApmQuery = { __typename?: 'Query', staff?: Array<{ __typename?: 'Staff', status?: Staff_Status | null | undefined, apmId: string, apmIdNew?: string | null | undefined, adminId: string, amka?: string | null | undefined, afm?: string | null | undefined, gender: Staff_Gender, birthdate?: Date | null | undefined, first: string, firstEn?: string | null | undefined, middle?: string | null | undefined, middleEn?: string | null | undefined, last: string, lastEn?: string | null | undefined, telHome?: string | null | undefined, telMobile?: string | null | undefined, telFax?: string | null | undefined, telOffice?: string | null | undefined, homeAddress?: string | null | undefined, nationalityId?: string | null | undefined, start?: Date | null | undefined, end?: Date | null | undefined, titleId: string, unitId: string, subUnitId?: string | null | undefined, unitDetails?: string | null | undefined, comment?: string | null | undefined, lastModifiedAtSource?: Date | null | undefined, lastSync?: Date | null | undefined, lastUpdated: Date, roles?: Array<{ __typename?: 'Roles', id: number, unit: { __typename?: 'Unit', name: string, nameEn?: string | null | undefined, id: string }, title: { __typename?: 'Title', id: string, nameEn: string, nameMale: string, nameFemale: string } }> | null | undefined, roles_expired?: Array<{ __typename?: 'RolesExpired', id: number, role: string, start?: Date | null | undefined, end?: Date | null | undefined, unit: { __typename?: 'Unit', name: string, nameEn?: string | null | undefined, id: string }, title: { __typename?: 'Title', id: string, nameEn: string, nameMale: string, nameFemale: string } }> | null | undefined } | null | undefined> | null | undefined };

export type ExpireRoleMutationVariables = Exact<{
  id: Scalars['Int'];
}>;


export type ExpireRoleMutation = { __typename?: 'Mutation', expireRole?: { __typename?: 'RolesExpired', id: number, apmId: string, unit: { __typename?: 'Unit', nameEn?: string | null | undefined }, title: { __typename?: 'Title', nameEn: string } } | null | undefined };

export type RestoreRoleMutationVariables = Exact<{
  id: Scalars['Int'];
}>;


export type RestoreRoleMutation = { __typename?: 'Mutation', restoreRole?: { __typename?: 'Roles', id: number, apmId: string, unit: { __typename?: 'Unit', nameEn?: string | null | undefined }, title: { __typename?: 'Title', nameEn: string } } | null | undefined };

export type AddRoleMutationVariables = Exact<{
  role: Scalars['ID'];
  apmId: Scalars['String'];
  unitId: Scalars['ID'];
}>;


export type AddRoleMutation = { __typename?: 'Mutation', addRole?: { __typename?: 'Roles', apmId: string, role: string, unit: { __typename?: 'Unit', nameEn?: string | null | undefined } } | null | undefined };

export const QueryDocument = gql`
    query Query {
  staff {
    apmId
    firstEn
    lastEn
  }
  units {
    id
    name
    nameEn
  }
  titles {
    id
    nameEn
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class QueryGQL extends Apollo.Query<QueryQuery, QueryQueryVariables> {
    document = QueryDocument;
    client = 'base';
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const SearchDocument = gql`
    query Search($unitId: ID, $titleId: [ID!], $name: String) {
  staff2(unitId: $unitId, titleId: $titleId, name: $name) {
    apmId
    firstEn
    lastEn
    first
    last
    roles {
      role
      unit {
        name
        nameEn
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class SearchGQL extends Apollo.Query<SearchQuery, SearchQueryVariables> {
    document = SearchDocument;
    client = 'base';
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const StaffWithApmDocument = gql`
    query StaffWithApm($apmId: String!) {
  staff(apmId: $apmId) {
    status
    apmId
    apmIdNew
    adminId
    amka
    afm
    gender
    birthdate
    first
    firstEn
    middle
    middleEn
    last
    lastEn
    telHome
    telMobile
    telFax
    telOffice
    homeAddress
    nationalityId
    start
    end
    titleId
    unitId
    subUnitId
    unitDetails
    comment
    lastModifiedAtSource
    lastSync
    lastUpdated
    roles {
      id
      unit {
        name
        nameEn
        id
      }
      title {
        id
        nameEn
        nameMale
        nameFemale
      }
    }
    roles_expired {
      id
      role
      start
      end
      unit {
        name
        nameEn
        id
      }
      title {
        id
        nameEn
        nameMale
        nameFemale
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class StaffWithApmGQL extends Apollo.Query<StaffWithApmQuery, StaffWithApmQueryVariables> {
    document = StaffWithApmDocument;
    client = 'base';
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ExpireRoleDocument = gql`
    mutation ExpireRole($id: Int!) {
  expireRole(id: $id) {
    id
    apmId
    unit {
      nameEn
    }
    title {
      nameEn
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class ExpireRoleGQL extends Apollo.Mutation<ExpireRoleMutation, ExpireRoleMutationVariables> {
    document = ExpireRoleDocument;
    client = 'base';
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const RestoreRoleDocument = gql`
    mutation RestoreRole($id: Int!) {
  restoreRole(id: $id) {
    id
    apmId
    unit {
      nameEn
    }
    title {
      nameEn
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class RestoreRoleGQL extends Apollo.Mutation<RestoreRoleMutation, RestoreRoleMutationVariables> {
    document = RestoreRoleDocument;
    client = 'base';
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const AddRoleDocument = gql`
    mutation AddRole($role: ID!, $apmId: String!, $unitId: ID!) {
  addRole(role: $role, apmId: $apmId, unitId: $unitId) {
    apmId
    role
    unit {
      nameEn
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class AddRoleGQL extends Apollo.Mutation<AddRoleMutation, AddRoleMutationVariables> {
    document = AddRoleDocument;
    client = 'base';
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }