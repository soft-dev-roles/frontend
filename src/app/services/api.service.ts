import { Injectable } from '@angular/core';
import {Apollo, ApolloBase, gql} from 'apollo-angular';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apollo: ApolloBase;

  constructor(private apolloProvider: Apollo) {
    this.apollo = this.apolloProvider.use('base');
  }

  getData() {
    return this.apollo.watchQuery({
      query: gql`
        query Query {
          staff {
            apmId
            firstEn
            lastEn
          }
        }
      `,
    });
  }
}
