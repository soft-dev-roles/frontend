import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(items: string[], searchKeyword: string) {
    searchKeyword = searchKeyword.normalize("NFD").replace(/[\u0300-\u036f]/g, '').toLocaleLowerCase();
    return items.filter(item =>
      item.normalize("NFD").replace(/[\u0300-\u036f]/g, '').toLocaleLowerCase().includes(searchKeyword)
    );
  }
}
