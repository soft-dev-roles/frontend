import { HttpHeaders } from '@angular/common/http';
import { Component, ContentChild, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { QueryRef } from 'apollo-angular';
import { Subscription } from 'rxjs';
import { SearchGQL, SearchQuery, SearchQueryVariables, StaffWithApmGQL } from '../services/generated-frontend';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  public result: SearchQuery['staff2'];

  private queryRef?: QueryRef<SearchQuery, SearchQueryVariables>;
  private querySub = new Subscription();

  constructor(
    private searchService: SearchGQL,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.queryRef = this.searchService.watch({}, {
      context: {headers: new HttpHeaders().set("Authorization", "0000001000095556")}
    });
    this.querySub = this.queryRef.valueChanges
      .subscribe((response) => {
        this.result = response.data.staff2;
      });
  }

  ngOnDestroy(): void {
    this.querySub.unsubscribe();
  }

  onFormChange(event: any) {
    this.queryRef?.refetch({
      unitId: event.selectedUnitId,
      titleId: event.selectedTitlesId,
      name: event.searchKeyword
    });
  }

  onRowClick(staff: any) {
    this.router.navigate(['/', staff.apmId]);
  }
}
