import { Pipe, PipeTransform } from '@angular/core';
import { StaffWithApmQuery } from './services/generated-frontend';


type StaffType = Exclude<StaffWithApmQuery['staff'], null | undefined>[number];

const listTypes = ['__typename', 'first', 'firstEn', 'last', 'lastEn', 'apmId', 'lastUpdated', 'roles'];

@Pipe({
  name: 'notInList'
})
export class NotInListPipe implements PipeTransform {

  transform(value: StaffType) {
    let returnValue: { [key: string]: any } = {};
    if (value)
      for (const key in value)
        if (!listTypes.includes(key))
          returnValue[key] = value[key as keyof typeof value];
    return returnValue;
  }

}
