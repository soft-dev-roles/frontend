import { HttpHeaders } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { QueryRef } from 'apollo-angular';
import { Subscription } from 'rxjs';
import { StaffWithApmGQL, StaffWithApmQuery, StaffWithApmQueryVariables } from '../services/generated-frontend';


type StaffType = Exclude<StaffWithApmQuery['staff'], null | undefined>[number];

@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.sass']
})
export class StaffComponent implements OnInit, OnDestroy {
  public apmId: string | null = null;
  public staff: StaffType;

  public hideAttr = ['__typename', 'first', 'firstEn', 'last', 'lastEn', 'apmId', 'lastUpdated'];
  
  private queryRef?: QueryRef<StaffWithApmQuery, StaffWithApmQueryVariables>;
  private subscription = new Subscription();

  constructor(
    private route: ActivatedRoute,
    private staffService: StaffWithApmGQL
  ) { }

  ngOnInit(): void {
    this.apmId = this.route.snapshot.paramMap.get('apmId');
    this.queryRef = this.staffService.watch({apmId: this.apmId!}, {
      context: {headers: new HttpHeaders().set("Authorization", "0000001000095556")}
    });
    this.subscription = this.queryRef.valueChanges
      .subscribe(response => {
        if (response.data.staff)
          this.staff = response.data.staff[0];
      });
  }

  get roles() {
    return this.staff?.roles as any;
  }

  get roles_expired() {
    return this.staff?.roles_expired as any;
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  reload() {
    console.log("reloading");
    this.queryRef?.refetch({apmId: this.apmId!});
  }
}
