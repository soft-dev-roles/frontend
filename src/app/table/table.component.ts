import { Component, ContentChild, Input, OnInit, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  @ContentChild('rowTemplate', { static: false })
  rowRef: TemplateRef<any> | null = null;
  
  @Input() header:  string[] = [];
  @Input() data: any;
  @Input() tableClass  = "";
  @Input() headerClass = "";
  @Input() mySmall = false;

  constructor() { }

  ngOnInit(): void {
  }

}
