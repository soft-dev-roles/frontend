import { Component, ElementRef, Input, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-roles-tabs',
  templateUrl: './roles-tabs.component.html',
  styleUrls: ['./roles-tabs.component.sass']
})
export class RolesTabsComponent implements OnInit {
  @Input() roles: { [key: string]: any }[] = [];
  @Input() roles_expired: { [key: string]: any }[] = [];
  @Input() isMale = false;
  @Input() apmId = "";

  @Output()
  reloadEvent = new EventEmitter();

  first = true;

  constructor() { }

  ngOnInit(): void {
  }

  get rolesLength() {
    return this.roles?.length ? this.roles?.length : 0;
  }

  get rolesExpiredLength() {
    return this.roles_expired?.length ? this.roles_expired?.length : 0;
  }

}
