import { Component, OnDestroy, OnInit, Output, EventEmitter } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { interval, Subject, Subscription } from 'rxjs';
import { throttle } from 'rxjs/operators';
import { QueryGQL, QueryQuery } from '../services/generated-frontend';

type unitQuery  = Exclude<QueryQuery['units'], null | undefined>[number];
// type titleQuery = Exclude<QueryQuery['titles'], null | undefined>[number];


@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.sass']
})
export class DropdownComponent implements OnInit, OnDestroy {
  private subscription1 = new Subscription();
  private subscription2 = new Subscription();
  private subject = new Subject<unitQuery>();
  public units:  QueryQuery['units'];
  public titles: QueryQuery['titles'];

  public group = this.formBuilder.group({
    selectedUnit: [],
    selectedTitle: [],
    searchKeyword: [],
    formArray: this.formBuilder.array([])
  });
  private formMask: boolean[] = [];

  @Output() formEvent = new EventEmitter<{
    selectedUnitId?: string,
    selectedTitlesId?: string[],
    searchKeyword?: string
  }>();

  constructor(
    private formBuilder: FormBuilder,
    private queryService: QueryGQL
  ) { }

  ngOnInit(): void {
    this.subscription1 = this.queryService.watch().valueChanges
      .subscribe((response) => {
        this.units  = response.data.units;
        this.titles = response.data.titles;
      });
    this.subscription2 = this.subject
      .subscribe(data => {
        console.log("got data");
      });
    this.group.get('selectedTitle')!.valueChanges
      .subscribe((newTitle) => {
        if (!this.formArray.controls.find((control) => control.value == newTitle) && newTitle) {
          this.formArray.push(this.formBuilder.control(newTitle));
          this.formMask.push(true);
        }
      });
    this.group.valueChanges
      .pipe(throttle(_ => interval(1000), {leading: false, trailing: true}))
      .subscribe(_ => {
        // const greekAndCoptic = /[\u0370-\u03ff]/;
        // const hasGreekAndCoptic = greekAndCoptic.test(this.group.get('searchKeyword')?.value);
        this.formEvent.emit({
          selectedUnitId: this.selectedUnitId,
          selectedTitlesId: this.activeTitlesId,
          searchKeyword: this.group.get('searchKeyword')?.value
        });
      });
  }

  ngOnDestroy(): void {
    this.subscription1.unsubscribe();
    this.subscription2.unsubscribe();
  }

  get formArray() {
    return this.group.get('formArray') as FormArray;
  }

  get activeTitlesId() {
    return this.formArray.controls
      .filter((_, index) => this.formMask[index])
      .map(control => control.value.id as string);
  }

  get selectedUnitId() {
    return this.group.get('selectedUnit')?.value?.id;
  }

  removeTitleForm(index: number) {
    this.formArray.removeAt(index);
    this.formMask.splice(index, 1);
    if (this.formArray.controls.length == 0) {
      this.group.get('selectedTitle')?.setValue(null);
    }
  }

  checkBoxChange(event: any, index: number) {
    if (event.target.checked) {
      this.formMask[index] = true;
    } else if (!event.target.checked) {
      this.formMask[index] = false;
    }
    this.group.patchValue({});
  }

  onClear() {
    this.formArray.clear();
    this.formMask = [];
    this.group.get('selectedTitle')?.setValue(null);
  }
}
