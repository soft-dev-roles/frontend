import { HttpHeaders } from '@angular/common/http';
import { Component, ElementRef, Input, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { AddRoleGQL, ExpireRoleGQL, RestoreRoleGQL } from '../services/generated-frontend';

@Component({
  selector: 'app-tab-content',
  templateUrl: './tab-content.component.html',
  styleUrls: ['./tab-content.component.sass']
})
export class TabContentComponent implements OnInit {
  @Input() identity = "";
  @Input() data: { [key: string]: any }[] = [];
  @Input() isMale = true;
  @Input() action: string = "";
  @Input() apmId = "";

  @ViewChild('toastSuccess')
  toastSuccess: ElementRef | null = null;

  @ViewChild('toastFail')
  toastFail: ElementRef | null = null;

  @Output()
  deleteSuccess = new EventEmitter();

  @Output()
  addSuccess = new EventEmitter();

  selected: any;

  constructor(
    private expireService: ExpireRoleGQL,
    private restoreService: RestoreRoleGQL,
    private addRoleService: AddRoleGQL
  ) { }

  ngOnInit(): void {
  }

  get length() {
    return this.data.length;
  }

  getRole(item: any) {
    return this.isMale ? item.title.nameMale : item.title.nameFemale;
  }

  selectItem(item: any) {
    const handle = this.toastSuccess?.nativeElement as Element;
    handle.classList.remove("show", "active");
    this.selected = item;
  }

  deleteAccept() {
    if (this.action === "Delete") {
      this.expireService.mutate({id: this.selected?.id}, {
        context: {headers: new HttpHeaders().set("Authorization", "0000001000090290")}
      }).subscribe((response) => {
          this.selected = response.data?.expireRole;
          const handle = this.toastSuccess?.nativeElement as Element;
          handle.classList.add("show", "active");
          setTimeout(() => {
            handle.classList.remove("show", "active");
          }, 4000);
          this.deleteSuccess.emit('');
      }, (error) => {
        const handle = this.toastFail?.nativeElement as Element;
        handle.classList.add("show", "active");
          setTimeout(() => {
            handle.classList.remove("show", "active");
          }, 4000);
      });
    } else if (this.action === "Restore") {
      this.restoreService.mutate({id: this.selected?.id}, {
        context: {headers: new HttpHeaders().set("Authorization", "0000001000090290")}
      }).subscribe((response) => {
          this.selected = response.data?.restoreRole;
          const handle = this.toastSuccess?.nativeElement as Element;
          handle.classList.add("show", "active");
          setTimeout(() => {
            handle.classList.remove("show", "active");
          }, 4000);
          this.deleteSuccess.emit('');
      }, (error) => {
        const handle = this.toastFail?.nativeElement as Element;
        handle.classList.add("show", "active");
          setTimeout(() => {
            handle.classList.remove("show", "active");
          }, 4000);
      });
    }
  }

  onAddRole(event: any) {
    this.addRoleService.mutate(
      {apmId: this.apmId, role: event.selectedTitle, unitId: event.selectedUnit},
      {context: {headers: new HttpHeaders().set("Authorization", "0000001000105512")}}
    ).subscribe(response => {
      console.log(response);
      this.addSuccess.emit('');
    }, error => {
      console.log(error);
    })
  }
}
